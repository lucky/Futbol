<!DOCTYPE html>
<html>
    <head>
        <title>Estadistica</title>
    </head>
    <body>
    <a href="index.php	">Inicio</a> 
	<table border="1">
		<thead>
		<tr>
			<th>Equipo</th>
			<th>PartidosJugados</th>
			<th>Partidos Ganados</th>
			<th>Partidos Perdidos</th>
			<th>Partidos Empatados</th>
			<th>Anotaciones</th>
			<th>Goles En Contra</th>
			<th>Goles A Favor</th>
			<th>Puntos</th>
		</tr>
		</thead>
		<tbody>
<?php
$base_de_datos = new SQLite3("squlite");
$resultados = $base_de_datos->query("SELECT * FROM partidos;");
while ($partido = $resultados->fetchArray()) {

?>
		<tr>
			<td> <?php echo $partido["fecha"]; ?></td>
			<td> <?php echo $partido["equipo"]; ?></td>
			<td> <?php echo $partido["goles_anotados"]; ?></td>
			<td> <?php echo $partido["goles_recibidos"]; ?></td>
		</tr>
<?php } ?>
		</tbody>	
	</table>

    </body>
</html>