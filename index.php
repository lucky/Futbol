<!DOCTYPE html>
<html>
    <head>
        <title>Partidos</title>
    </head>
    <body>
    <a href="formulario_partido.php">Añadir Partido </a><a href="estadistica.php">Avance De Partidos</a>
	<table border="1">
		<thead>
		<tr>
			<th>Fecha</th>
			<th>Equipo</th>
			<th>Goles Anotados</th>
			<th>Goles Recibidos</th>
		</tr>
		</thead>
		<tbody>
<?php
$base_de_datos = new SQLite3("squlite");
$resultados = $base_de_datos->query("SELECT * FROM partidos;");
while ($partido = $resultados->fetchArray()) {

?>
		<tr>
			<td> <?php echo $partido["fecha"]; ?></td>
			<td> <?php echo $partido["equipo"]; ?></td>
			<td> <?php echo $partido["goles_anotados"]; ?></td>
			<td> <?php echo $partido["goles_recibidos"]; ?></td>
		</tr>
<?php } ?>
		</tbody>	
	</table>

    </body>
</html>
